package ru.tsc.gulin.tm.exception.system;

import ru.tsc.gulin.tm.exception.AbstractException;

public class AuthorisationDeniedException extends AbstractException {

    public AuthorisationDeniedException() {
        super("Error! Login or password is incorrect...");
    }

}
