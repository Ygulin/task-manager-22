package ru.tsc.gulin.tm.api.model;

import ru.tsc.gulin.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
