package ru.tsc.gulin.tm.api.repository;

import ru.tsc.gulin.tm.enumerated.Sort;
import ru.tsc.gulin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    List<M> findAll(String userId, Sort sort);

    M add(String userId, M model);

    M findOneByIndex(String userId, Integer index);

    M findOneById(String userId, String id);

    M remove(String userId, M model);

    M removeByIndex(String userId, Integer index);

    M removeById(String userId, String id);

    void clear(String userId);

    boolean existsById(String userId, String id);

    long getSize(String userId);

}
